package de.fhg.fokus.edp.media_type_detector;

import de.fhg.fokus.edp.media_type_detector.model.MediaTypeDetectRequest;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static de.fhg.fokus.edp.media_type_detector.Constants.*;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

    private JsonObject config;

    @Override
    public void start() {
        LOG.info("Launching Media Type Detector...");

        // startup is only successful if no step failed
        Future<Void> steps = loadConfig()
                .compose(handler -> bootstrapVerticles())
                .compose(handler -> startServer());

        steps.setHandler(handler -> {
            if (handler.succeeded()) {

                LOG.info("Media Type Detector successfully launched");
            } else {
                handler.cause().printStackTrace();
                LOG.error("Failed to launch Media Type Detector: " + handler.cause());
            }
        });
    }

    private Future<Void> loadConfig() {
        Future<Void> future = Future.future();

        ConfigRetriever configRetriever = ConfigRetriever.create(vertx);

        configRetriever.getConfig(handler -> {
            if (handler.succeeded()) {
                config = handler.result();
                LOG.info(config.encodePrettily());
                future.complete();
            } else {
                future.fail("Failed to load config: " + handler.cause());
            }
        });

        configRetriever.listen(change ->
                config = change.getNewConfiguration());

        return future;
    }

    private CompositeFuture bootstrapVerticles() {
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(config)
                .setWorkerPoolName("extractor-pool")
                .setMaxWorkerExecuteTime(30)
                .setMaxWorkerExecuteTimeUnit(TimeUnit.MINUTES)
                .setWorker(true);

        List<Future> deploymentFutures = new ArrayList<>();
        deploymentFutures.add(startVerticle(options, MediaTypeDetectorVerticle.class.getName()));

        return CompositeFuture.join(deploymentFutures);
    }

    private Future<Void> startServer() {
        Future<Void> startFuture = Future.future();
        Integer port = config.getInteger(Constants.ENV_APPLICATION_PORT, 8121);

        OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", handler -> {
            if (handler.succeeded()) {
                OpenAPI3RouterFactory routerFactory = handler.result();
                RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true).setMountValidationFailureHandler(true);
                routerFactory.setOptions(options);

                routerFactory.addHandlerByOperationId("detectMediaType", this::handleRequest);

                Router router = routerFactory.getRouter();
                router.route("/*").handler(StaticHandler.create());

                HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(port));
                server.requestHandler(router).listen();

                LOG.info("Server successfully launched on port [{}]", port);
                startFuture.complete();
            } else {
                // Something went wrong during router factory initialization
                LOG.error("Failed to start server at [{}]: {}", port, handler.cause());
                startFuture.fail(handler.cause());
            }
        });

        return startFuture;
    }

    private void handleRequest(RoutingContext context) {
        MediaTypeDetectRequest request = new MediaTypeDetectRequest();
        request.setCallbackUrl(context.request().formAttributes().get(FORM_PARAM_CALLBACK_URL));
        request.setFiles(context.fileUploads()
                .stream()
                .map(this::moveAndRenameFile)
                .collect(Collectors.toList()));

        LOG.debug("Received {}", request);

        if (request.getCallbackUrl() != null
                && !request.getCallbackUrl().isEmpty()
                && !request.getFiles().isEmpty()) {

            vertx.eventBus().send(ADDRESS_CHECK_MEDIA_TYPE, Json.encode(request));

            context.response()
                    .setStatusCode(202)
                    .end();
        } else {
            context.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type", "application/json")
                    .end(new JsonObject().put("message", "Missing form values").encode());
        }
    }

    private Future<Void> startVerticle(DeploymentOptions options, String className) {
        Future<Void> future = Future.future();

        vertx.deployVerticle(className, options, handler -> {
            if (handler.succeeded()) {
                future.complete();
            } else {
                LOG.error("Failed to deploy verticle [{}] : {}", className, handler.cause());
                future.fail("Failed to deploy [" + className + "] : " + handler.cause());
            }
        });

        return future;
    }

    private Path moveAndRenameFile(FileUpload fileUpload) {
        Path path = Paths.get(config().getString(ENV_WORK_DIR, "/tmp"), fileUpload.fileName());
        vertx.fileSystem().moveBlocking(fileUpload.uploadedFileName(), path.toString());
        return path;
    }
}
