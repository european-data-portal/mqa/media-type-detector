package de.fhg.fokus.edp.media_type_detector.model;

public enum ArchiveType {

    NONE(""),
    ZIP("application/zip"),
    TAR("application/x-gtar"),
    TAR_GZ("application/gzip"),
    _7Z("application/x-7z-compressed"),
    RAR("application/vnd.rar");

    private final String mediaType;

    ArchiveType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaType() {
        return this.mediaType;
    }
}
