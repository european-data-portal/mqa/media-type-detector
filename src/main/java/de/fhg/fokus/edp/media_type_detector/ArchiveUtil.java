package de.fhg.fokus.edp.media_type_detector;

import de.fhg.fokus.edp.media_type_detector.model.ArchiveType;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.spi.FileSystemProvider;
import java.util.ArrayList;
import java.util.List;

class ArchiveUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ArchiveUtil.class);

    static List<Path> extract(Path inputFile, Path outputDir, ArchiveType type) throws IOException {
        LOG.debug("Extracting [{}] [{}] into [{}]", type, inputFile, outputDir);

        if (!Files.exists(outputDir))
            Files.createDirectory(outputDir);

        FileSystemProvider fsProvider = outputDir.getFileSystem().provider();

        switch (type) {
            case ZIP:
                try (ZipArchiveInputStream archiveInputStream = new ZipArchiveInputStream(fsProvider.newInputStream(inputFile))) {
                    return extractArchive(archiveInputStream, outputDir);
                }
            case TAR:
                try (TarArchiveInputStream archiveInputStream = new TarArchiveInputStream(fsProvider.newInputStream(inputFile))) {
                    return extractArchive(archiveInputStream, outputDir);
                }
            case TAR_GZ:
                try (TarArchiveInputStream archiveInputStream = new TarArchiveInputStream(new GzipCompressorInputStream(fsProvider.newInputStream(inputFile)))) {
                    return extractArchive(archiveInputStream, outputDir);
                }
            case _7Z:
                throw new UnsupportedOperationException("7z is currently not supported");
            case RAR:
                throw new UnsupportedOperationException("RAR is currently not supported");
            default:
                return new ArrayList<>();
        }

    }

    private static List<Path> extractArchive(ArchiveInputStream archiveInputStream, Path outputDir) throws IOException {
        List<Path> archiveContent = new ArrayList<>();

        ArchiveEntry entry;
        while ((entry = archiveInputStream.getNextEntry()) != null) {
            if (entry.isDirectory()) {
                continue;
            }

            File currentFile = new File(outputDir.toString(), entry.getName());
            File parent = currentFile.getParentFile();
            LOG.debug("Extracting file [{}]", currentFile.getAbsolutePath());

            if (!parent.exists() && !parent.mkdirs()) {
                throw new IOException("Failed to create dirs in [" + parent + "]");
            }

            IOUtils.copy(archiveInputStream, new FileOutputStream(currentFile));
            archiveContent.add(Paths.get(currentFile.toURI()));
        }

        return archiveContent;
    }

}
